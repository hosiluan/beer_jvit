//
//  BeerMKPointAnnotation.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/09/28.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import UIKit
import MapKit

class StoreMKPointAnnotation: MKPointAnnotation {

    var pinImage: UIImage!
    var storeIndex: Int!
    
    init(image: UIImage, index: Int) {
        self.pinImage = image
        self.storeIndex = index
    }
}
