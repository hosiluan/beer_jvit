//
//  Illustrator.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/10/11.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import UIKit


enum IconType {
    case small
    case big
}

class Illustrator {
    
    static var fontSize = 17 // default size
    
    static func renderPriceText(text: NSString, image: UIImage, color: UIColor) -> UIImage {
        // 事前準備
        let imageWidth = image.size.width
        let imageHeight = image.size.height
        
        // FontSizeを動的に調整（3桁のケースと4桁のケース／数字によって（ex. ”1”と”3”とで）Textの横幅が異なるのに対応するため）
        var textFontAttributes: [NSAttributedString.Key: Any] = [
            NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(self.fontSize)),
            NSAttributedStringKey.foregroundColor: color
        ]
        var textWidth = text.size(withAttributes: textFontAttributes).width
        while textWidth > imageWidth * 0.8 { // 余白を考慮するためimageWidthに0.8を掛けている
            self.fontSize -= 1
            textFontAttributes.updateValue(UIFont.systemFont(ofSize: CGFloat(self.fontSize)), forKey: NSAttributedStringKey.font)
            textWidth = text.size(withAttributes: textFontAttributes).width
        }
        let textHeight = text.size(withAttributes: textFontAttributes).height
        let textRect = CGRect(x: (imageWidth - textWidth) / 2,
                              y: (imageHeight * 0.9 - textHeight) / 2, // imageHeightに0.9を掛けるのはy軸方向の描画位置を調整する必要があるため（アイコン形状が吹き出しなので）
            width: imageWidth,
            height: imageHeight)
        
        // 描画開始
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x: 0, y: 0, width: imageWidth, height: imageHeight))
        text.draw(in: textRect, withAttributes: textFontAttributes)
        let newImage: UIImage! = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    
    static func addChepestFlag(baseImage: UIImage, type: IconType) -> UIImage {
        let cheapestImage = R.image.saiyasu()!
        
        let baseWidth = baseImage.size.width
        let baseHeight = baseImage.size.height
        let cheapestWidth = cheapestImage.size.width
        let cheapestHeight = cheapestImage.size.height
        
        let width = baseWidth + cheapestWidth
        var height: CGFloat
        if type == IconType.big {
            height = baseHeight * 0.8 + cheapestHeight
        } else {
            height = baseHeight * 0.55 + cheapestHeight
        }
        
        let baseX: CGFloat = width / 2 - baseWidth / 2
        let baseY: CGFloat = height - baseHeight
        let cheapX: CGFloat = width - cheapestWidth
        let cheapY: CGFloat = 0
        
        // 描画開始
        UIGraphicsBeginImageContext(CGSize(width: width, height: height))
        baseImage.draw(in: CGRect(x: baseX, y: baseY, width: baseWidth, height: baseHeight))
        cheapestImage.draw(in: CGRect(x: cheapX, y: cheapY, width: cheapestWidth, height: cheapestHeight))
        
        let newImage: UIImage! = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }
}
