//
//  SlideMenuViewController.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/09/20.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import Foundation
import UIKit


private class MenuItem {
    
    var layoutType: LayoutType!
    var title: String!
    var textColor: UIColor!
    var image: UIImage!
    
    init(title: String, type: LayoutType, color: UIColor, image: UIImage) {
        self.title = title
        self.layoutType = type
        self.textColor = color
        self.image = image
    }
}


class SlideMenuViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let menuItems = [
        MenuItem(title: "お気に入り", type: LayoutType.plane,
                 color: UIColor.black, image: R.image.ic_arrow()!),
        MenuItem(title: "閲覧履歴", type: LayoutType.plane,
                 color: UIColor.black, image: R.image.ic_arrow()!),
        MenuItem(title: "よく飲むエリアの設定", type: LayoutType.showContent,
                 color: UIColor.black, image: R.image.ic_arrow()!),
        MenuItem(title: "サービス利用規約", type: LayoutType.plane,
                 color: UIColor.gray, image: R.image.ic_arrow_gray()!),
        MenuItem(title: "アプリケーションプライバシーポリシー", type: LayoutType.plane,
                 color: UIColor.gray, image: R.image.ic_arrow_gray()!),
        MenuItem(title: "注意事項", type: LayoutType.plane,
                 color: UIColor.gray, image: R.image.ic_arrow_gray()!),
        MenuItem(title: "ヘルプ", type: LayoutType.plane,
                 color: UIColor.gray, image: R.image.ic_arrow_gray()!)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(R.nib.itemPlaneCell)
        self.tableView.register(R.nib.itemShowContentCell)
    }
}


extension SlideMenuViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let menuItem = menuItems[indexPath.row]
        
        if menuItem.layoutType! == .plane {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.itemPlaneCell, for: indexPath)!
            cell.setViewElements(title: menuItem.title, color: menuItem.textColor, image: menuItem.image)
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.itemShowContentCell, for: indexPath)!
            cell.setViewElements(title: menuItem.title)
            //cell.setViewElements(title: menuItem.title, color: menuItem.textColor, image: menuItem.image)
            return cell
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.menuItems.count
    }
}


extension SlideMenuViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row <= 2 {
            return 50
        } else {
            return 40
        }
    }
}
