//
//  ListPageTopControllerCell.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/09/21.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import UIKit


class ListPageTopControllerCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    // awakeFromNib(IBOutletやIBActionがロードされた後に呼び出される）
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
