//
//  ColorPicker.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/10/11.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import UIKit


class ColorPicker {
    
    static func pickBrandColor(brandName: String) -> UIColor {
        guard let brandName = BrandNames(rawValue: brandName) else {
            return UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        }
        
        switch brandName {
        case .asahi:
            return UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        case .ebisu:
            return UIColor(red: 204/255, green: 0/255, blue: 0/255, alpha: 1)
        case .kirin:
            return UIColor(red: 218/255, green: 78/255, blue: 0/255, alpha: 1)
        case .molts:
            return UIColor(red: 254/255, green: 200/255, blue: 42/255, alpha: 1)
        case .sapporo:
            return UIColor(red: 254/255, green: 200/255, blue: 42/255, alpha: 1)
        case .other:
            return UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        }
    }
}
