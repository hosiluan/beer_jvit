//
//  SearchPageViewController.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/10/15.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import UIKit

class SearchPageViewController: UIViewController {

    @IBOutlet weak var radioSmokeUnspecified: RadioButton!
    @IBOutlet weak var radioSmokeSeparate: RadioButton!
    @IBOutlet weak var radioSmokeUnable: RadioButton!
    @IBOutlet weak var radioSmokeAble: RadioButton!
    
    @IBOutlet weak var checkBoxKoshitu: CheckBox!
    @IBOutlet weak var checkBoxZashiki: CheckBox!
    @IBOutlet weak var checkBoxKashikiri: CheckBox!
    @IBOutlet weak var checkBoxHorigotatsu: CheckBox!
    
    var radioTmp: RadioButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        radioSmokeUnspecified.isSelected = true
        radioTmp = radioSmokeUnspecified
        
        checkBoxKoshitu.isChecked = false
        checkBoxZashiki.isChecked = false
        checkBoxKashikiri.isChecked = false
        checkBoxHorigotatsu.isChecked = false
    }
    

    @IBAction func didSelectRadioButton(_ sender: RadioButton) {
        if sender == radioTmp {
            return
        }
        radioTmp.isSelected = false
        sender.isSelected = !sender.isSelected
        radioTmp = sender
    }
    
    
    @IBAction func onClickSearch(_ sender: UIButton) {
        var smoke = ""
        smoke += radioSmokeSeparate.isSelected ? "1" : ""
        smoke += radioSmokeUnable.isSelected ? "0" : ""
        smoke += radioSmokeAble.isSelected ? "2" : ""

        var seat = ""
        seat += checkBoxKoshitu.isChecked ? "&personal=1" : ""
        seat += checkBoxKashikiri.isChecked ? "&reserved=1" : ""
        seat += checkBoxZashiki.isChecked ? "&zashiki=1" : ""
        seat += checkBoxHorigotatsu.isChecked ? "&horigotatsu=1" : ""
        
        print("smoke & seat: ", smoke, seat)
    }
    
    
    @IBAction func didSelectCheckBox(_ sender: UIButton) {
        guard let checkBox = sender as? CheckBox else {
            return
        }
        checkBox.isChecked = checkBox.isChecked ? false : true
    }
}
