//
//  CheapestFinder.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/10/11.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

class CheapestFinder {
    var cheapestPrice: Int?
    var pleaseUpdateFlag: Bool = false
    
    func setCheapest(stores: [Store]) {
        if pleaseUpdateFlag {
            var prices: [Int] = []
            for store in stores {
                prices.append(Int(store.price!))
            }
            cheapestPrice = getMinimumPrice(prices: prices)
        }
    }
    
    private func getMinimumPrice(prices: [Int]) -> Int {
        var minPrice = 10000
        for price in prices where price < minPrice {
            minPrice = price
        }
        return minPrice
    }
}
