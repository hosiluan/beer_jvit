//
//  TypeDefine.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/09/27.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import Foundation

enum LayoutType {
    case plane, showContent
}
