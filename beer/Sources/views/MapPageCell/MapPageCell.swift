//
//  MapPageCell.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/10/01.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import UIKit


class MapPageCell: UICollectionViewCell {

    @IBOutlet weak var storeImage: UIImageView!
    @IBOutlet weak var imageBrand: UIImageView!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var favorite: UIImageView!
    @IBOutlet weak var storeName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setViewElements(stores: [Store]?, indexPath: IndexPath) {
        guard let stores = stores else {
            return
        }
        let store = stores[indexPath.row]
        storeImage?.kf.setImage(with: URL(string: stores[indexPath.row].imageUrl!))
        storeName?.text = store.storeName
        price?.text = String(store.price!)
        print(store.brand!)
        imageBrand.image = ImagePicker.pickBrandIconForMapCell(brandName: store.brand!) // FixMe: URLが古くimageが返されないことがある。エラーにはならない
        favorite.image = R.image.ic_favorite_on()
    }
}
