//
//  ListPageMiniCell.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/09/21.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher


class ListPageMiniCell: UICollectionViewCell {
    
    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var storeName: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var brandImage: UIImageView!
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var favorite: UIImageView!
    @IBOutlet weak var gradationLayerView: UIView!
    
    
    // awakeFromNib(IBOutletやIBActionがロードされた後に呼び出される）
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let gradientLayer = CAGradientLayer()
        if let bounds = gradationLayerView?.bounds {
            gradientLayer.frame = bounds
        }
        let topColor = UIColor(red: 100/255, green: 100/255, blue: 100/255, alpha: 0.0).cgColor
        let bottomColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1.0).cgColor
        gradientLayer.colors = [topColor, bottomColor]
        gradientLayer.startPoint = CGPoint(x: 0.5, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0.5, y: 1)
        gradationLayerView?.layer.sublayers?.removeAll() // Gradationの上書きを防ぐため、addSublayerの前にsublayersをremove。
        gradationLayerView?.layer.addSublayer(gradientLayer)
    }
    
    
    func setViewElements(stores: [Store]?, indexPath: IndexPath) {
        if let stores = stores {
            let store = stores[indexPath.row]
            background?.kf.setImage(with: URL(string: store.beerUrl!))
            storeName?.text = store.storeName
            price?.text = String(store.price!)
            brandImage.image = ImagePicker.pickBrandIconForMapCell(brandName: store.brand!)
            category?.text = store.category
            distance?.text = String(store.distance!)
            favorite.image = R.image.ic_favorite_on()
        }
    }
}
