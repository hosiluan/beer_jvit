//
//  HorizontalCollectionViewLayout.swift
//  beer
//
//  Created by Masanao Matsuda on 2018/10/01.
//  Copyright © 2018年 NTT Resonant Inc. All rights reserved.
//

// 目的：Swipe動作の挙動を制御すること


import UIKit
import MapKit


class HorizontalCollectionViewLayout: UICollectionViewFlowLayout {
    
    var currentPosition: Int! // [必須] CollectionViewのページングのため
    var itemCount: Int!       // [必須] CollectionViewのページングのため
    var onSwipe: ((Int) -> Void)?
    
    let kFlickVelocityThreshold: CGFloat = 0.2
    var scrollingThreshold: CGFloat!
    let centerOfScreenX = UIScreen.main.bounds.width / 2
    let itemWidth = UIScreen.main.bounds.width * 0.85
    let itemHeight: CGFloat = 110 // StoryBoard にて設定したCollectionViewのHeightを基準として設定
    let insetTopAndBottom: CGFloat = 2
    let insetLeftAndRight = UIScreen.main.bounds.width * 0.15 / 2 // itemWidth が　0.85 であるため
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.itemSize = CGSize(width: itemWidth, height: itemHeight)
        self.scrollDirection = .horizontal
        self.sectionInset = UIEdgeInsets(top: insetTopAndBottom, left: insetLeftAndRight, bottom: insetTopAndBottom, right: insetLeftAndRight)
        self.scrollingThreshold = itemWidth + (self.minimumLineSpacing / 2) - centerOfScreenX
    }

    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {

        if fabs(velocity.x) > kFlickVelocityThreshold {
            if velocity.x > 0 {
                if currentPosition < itemCount - 1 {
                    currentPosition += 1
                    print("CurrentPosition increment: ", currentPosition)
                }
            
                // ViewControllerに処理を返す（SwipeをトリガーにMapのカメラポジションを該当店舗に移すため）
                onSwipe?(currentPosition)
                
                let nextX = (itemWidth + self.minimumLineSpacing) * CGFloat(currentPosition)
                return CGPoint(x: nextX, y: proposedContentOffset.y)
            } else {
                if currentPosition > 0 {
                    currentPosition -= 1
                    print("CurrentPosition decrement : ", currentPosition)
                }
                
                // ViewControllerに処理を返す（SwipeをトリガーにMapのカメラポジションを該当店舗に移すため）
                onSwipe?(currentPosition)
                
                let previousX = (itemWidth + self.minimumLineSpacing) * CGFloat(currentPosition)
                return CGPoint(x: previousX, y: proposedContentOffset.y)
            }
        } else {
            // Swipe途中で指を離したケース
            // 仕様：指を離す時点で、最も表示面積が広いCellが中心に吸い付くような仕様。
            // 実現方法：最も表示面積が広い状態とは、セルとセルの間がちょうど画面の中央を跨ぐことと定義して、以下計算。
            
            var currentX = insetLeftAndRight // CellのIndexが０の場合はinsetの距離を適用する
            if currentPosition != 0 {
                currentX = (itemWidth + self.minimumLineSpacing) * CGFloat(currentPosition)
            }
            
            let subtract = proposedContentOffset.x - currentX
            if subtract < -scrollingThreshold {
                print("previous!")
                
                let previousX = (itemWidth + self.minimumLineSpacing) * CGFloat(currentPosition - 1)
                if currentPosition > 0 {
                    currentPosition -= 1
                    print("CurrentPosition decrement : ", currentPosition)
                }
                onSwipe?(currentPosition)
                return CGPoint(x: previousX, y: proposedContentOffset.y)
                
            } else if subtract < scrollingThreshold {
                print("stay!")
                
                let stayX = (itemWidth + self.minimumLineSpacing) * CGFloat(currentPosition)
                print("CurrentPosition stay : ", currentPosition, "\n")
                return CGPoint(x: stayX, y: proposedContentOffset.y)
                
            } else {
                print("next!")
                
                let nextX = (itemWidth + self.minimumLineSpacing) * CGFloat(currentPosition + 1)
                if currentPosition < itemCount - 1 {
                    currentPosition += 1
                    print("CurrentPosition increment: ", currentPosition, "\n")
                }
                onSwipe?(currentPosition)
                return CGPoint(x: nextX, y: proposedContentOffset.y)
            }
        }
    }
}
